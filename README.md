# Clone-And-NPM

A docker image that clones a git repo, installs npm packages, and then runs node.

## Usage

1. Clone
  `git clone https://gitlab.com/pard/clone-and-npm`
2. Build
  `docker build -t clone-and-npm ./`
3. Run
  `docker run clone-and-npm https://git.url.example/foo/bar relative/path/to/file.js`
