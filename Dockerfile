FROM alpine as giter
RUN apk update && apk add git npm
COPY ./run.sh /
WORKDIR /app
ENTRYPOINT ["/bin/sh", "/run.sh"]
